/**
 * Các bước thực hiện
 * 1.dàn layout
 * 2. xác định dữ liệu thay đổi và lưu vào state
 * 3. Lấy data  trong state đi binding ra jsx
 * 4. render sản phẩm
 * 5. Xây dựng chứ năng xem chi tiêts
 * 6. xây dựng chứ năng thêm vào giỏ hàng
 * 7. xây dựng chứ năng tăng giảm số lượng
 * 8. xây dựng chứ năng xóa sản phẩm
 * 9. Hiển thị tổng sản phẩm trong giỏ hàng
 */

import React, { Component } from "react";

import danhSachSanPhan from "./data.json";
import Modal from "./Modal";
import SanPham from "./SanPham";

export default class BaiTapGioHang extends Component {
  state = {
    sanPhamChiTiet: {
      maSP: 1,
      tenSP: "VinSmart Live",
      manHinh: 'AMOLED, 6.2", Full HD+',
      heDieuHanh: "Android 9.0 (Pie)",
      cameraTruoc: "20 MP",
      cameraSau: "Chính 48 MP & Phụ 8 MP, 5 MP",
      ram: "4 GB",
      rom: "64 GB",
      giaBan: 5700000,
      hinhAnh: "./img/vsphone.jpg",
    },

    cardList: [],
  };

  handleSPChiTiet = (sanPhamChiTiet) => {
    this.setState({
      sanPhamChiTiet,
    });
  };

  handleCardList = (sanPham) => {
    //! Xét sp đã tồn tại chưa?
    const index = this.state.cardList.findIndex((card) => {
      return card.maSP === sanPham.maSP;
    });
    let cardList = [...this.state.cardList];
    if (index !== -1) {
      //? tìm thấy => tăng số lượng
      cardList[index].soLuong += 1;
    } else {
      //? k tìm thấy => thêm vào mảng +
      const newCard = { ...sanPham, soLuong: 1 };
      cardList = [...this.state.cardList, newCard];
    }
    this.setState({
      cardList,
    });
  };

  handleDeleteCard = (sanPhamXoa) => {
    // console.log(sanPhamXoa);
    // let cardList = [...this.state.cardList];
    // console.log("truoc xoa ->", cardList);
    let deleteSP = this.state.cardList.filter(
      (sanPham) => sanPham.maSP !== sanPhamXoa
    );
    console.log("sau xoa ->", deleteSP);
    this.setState({
      cardList: deleteSP,
    });
  };

  handlePlus = (soLuong) => {
    const index = this.state.cardList.findIndex((card) => {
      return card.maSP === soLuong.maSP;
    });

    let cardList = [...this.state.cardList];
    cardList[index].soLuong += 1;
    console.log(cardList);
    this.setState({
      cardList,
    });
    // console.log("cong", index);
  };

  handleMinus = (soLuong) => {
    const index = this.state.cardList.findIndex((card) => {
      return card.maSP === soLuong.maSP;
    });

    let cardList = [...this.state.cardList];
    if (cardList[index].soLuong <= 1) {
      return;
    } else {
      cardList[index].soLuong -= 1;
    }
    console.log(cardList);
    this.setState({
      cardList,
    });
    // console.log("cong", index);
  };

  renderDanhSachSanPhan = () => {
    return danhSachSanPhan.map((sanPham, i) => {
      return (
        <div className="col-sm-4" key={i}>
          <SanPham
            product={sanPham}
            xuLyChiTiet={this.handleSPChiTiet}
            xuLySanPham={this.handleCardList}
          />
        </div>
      );
    });
  };

  tongTien = () => {
    let tongTien = this.state.cardList.reduce(
      (tongTien, sanPham, i) => (tongTien += sanPham.soLuong * sanPham.giaBan),
      0
    );
    return tongTien;
  };

  render() {
    let total = this.state.cardList.reduce(
      (total, sanPham, i) => (total += sanPham.soLuong),
      0
    );
    return (
      <section className="container">
        <h3 className="title text-center">Bài tập giỏ hàng</h3>
        <div className="container text-center my-2">
          <button
            className="btn btn-danger "
            data-toggle="modal"
            data-target="#modelId"
          >
            Giỏ hàng ({total} - {this.tongTien().toLocaleString()})
          </button>
        </div>
        <div className="container danh-sach-san-pham">
          <div className="row">{this.renderDanhSachSanPhan()}</div>
        </div>

        <Modal
          cardList={this.state.cardList}
          xuLyXoaSanPham={this.handleDeleteCard}
          xuLyThemSoLuong={this.handlePlus}
          xuLyXoaSoLuong={this.handleMinus}
        />

        <div className="row mt-5">
          <div className="col-sm-5">
            <img
              className="img-fluid"
              src={this.state.sanPhamChiTiet.hinhAnh}
              alt=""
            />
          </div>
          <div className="col-sm-7">
            <h3>Thông số kỹ thuật</h3>
            <table className="table">
              <tbody>
                <tr>
                  <td>Màn hình</td>
                  <td>{this.state.sanPhamChiTiet.manHinh}</td>
                </tr>
                <tr>
                  <td>Hệ điều hành</td>
                  <td>{this.state.sanPhamChiTiet.heDieuHanh}</td>
                </tr>
                <tr>
                  <td>Camera trước</td>
                  <td>{this.state.sanPhamChiTiet.cameraTruoc}</td>
                </tr>
                <tr>
                  <td>Camera sau</td>
                  <td>{this.state.sanPhamChiTiet.cameraSau}</td>
                </tr>
                <tr>
                  <td>RAM</td>
                  <td>{this.state.sanPhamChiTiet.ram}</td>
                </tr>
                <tr>
                  <td>ROM</td>
                  <td>{this.state.sanPhamChiTiet.rom}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </section>
    );
  }
}
