import React, { Component } from "react";

export default class CardItem extends Component {
  render() {
    const { card } = this.props;
    let totalPrice = card.giaBan * card.soLuong;
    // console.log(totalPrice);
    return (
      <tr className="card-item">
        <td>{card.maSP}</td>
        <td>{card.tenSP}</td>
        <td>
          <img src={card.hinhAnh} width={50} alt="" />
        </td>
        <td>
          <button onClick={() => this.props.xoaSoLuong(card)}>-</button>
          {card.soLuong}
          <button onClick={() => this.props.themSoLuong(card)}>+</button>
        </td>
        <td>{card.giaBan.toLocaleString()}</td>
        <td>{totalPrice.toLocaleString()}</td>
        <td>
          <button
            className="btn btn-danger"
            onClick={() => this.props.xoaSanPham(card.maSP)}
          >
            Delete
          </button>
        </td>
      </tr>
    );
  }
}
