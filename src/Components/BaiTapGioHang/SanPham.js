import React, { Component } from "react";

export default class SanPham extends Component {
  handleSanPham = () => {
    const currentProduct = this.props.product;
    this.props.xuLyChiTiet(currentProduct);
  };

  render() {
    return (
      <div className="card">
        <img className="card-img-top" src={this.props.product.hinhAnh} alt="" />
        <div className="card-body">
          <h4 className="card-title">{this.props.product.tenSP}</h4>
          <button className="btn btn-success" onClick={this.handleSanPham}>
            Chi tiết
          </button>
          <button
            className="btn btn-danger"
            onClick={() => {
              this.props.xuLySanPham(this.props.product);
            }}
          >
            Thêm giỏ hàng
          </button>
        </div>
      </div>
    );
  }
}
