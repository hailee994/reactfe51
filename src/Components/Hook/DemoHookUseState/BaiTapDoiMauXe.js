import React, { useState } from "react";
//? const arr = []; các giá trị không phải state, không cần render lại khi setState => để ở ngoài component

export default function BaiTapDoiMauXe(props) {
  // const [state, setState] = useState({
  //   imgUrl: "./img/car/products/red-car.jpg",
  // });

  const [state, setState] = useState("./img/car/products/red-car.jpg");
  console.log(state);

  const handleChangeColor = (color) => {
    const newUrl = `./img/car/products/${color}-car.jpg`;
    setState(newUrl);
  };

  return (
    <section className="show-room">
      <h2 className="text-center">Bài Tập Chọn Màu Xe</h2>
      <div className="container">
        {/* <div className="chose__color d-flex justify-content-around">
          <button
            className="btn"
            onClick={() =>
              handleChangeColor("./img/car/products/black-car.jpg")
            }
          >
            <img
              src="./img/car/icons/icon-black.jpg"
              alt="hinh"
              style={{ width: 50 }}
            />
          </button>
          <button
            className="btn"
            onClick={() => handleChangeColor("./img/car/products/red-car.jpg")}
          >
            <img
              src="./img/car/icons/icon-red.jpg"
              alt="hinh"
              style={{ width: 50 }}
            />
          </button>
          <button
            className="btn"
            onClick={() =>
              handleChangeColor("./img/car/products/silver-car.jpg")
            }
          >
            <img
              src="./img/car/icons/icon-silver.jpg"
              alt="hinh"
              style={{ width: 50 }}
            />
          </button>
          <button
            className="btn"
            onClick={() =>
              handleChangeColor("./img/car/products/steel-car.jpg")
            }
          >
            <img
              src="./img/car/icons/icon-steel.jpg"
              alt="hinh"
              style={{ width: 50 }}
            />
          </button>
        </div> */}

        <div className="row">
          <div className="col-3">
            <button
              style={{ backgroundColor: "black", color: "#fff" }}
              onClick={() => handleChangeColor("black")}
            >
              black
            </button>
          </div>

          <div className="col-3">
            <button
              style={{ backgroundColor: "red" }}
              onClick={() => handleChangeColor("red")}
            >
              red
            </button>
          </div>

          <div className="col-3">
            <button
              style={{ backgroundColor: "silver" }}
              onClick={() => handleChangeColor("silver")}
            >
              silver
            </button>
          </div>

          <div className="col-3">
            <button
              style={{ backgroundColor: "steel" }}
              onClick={() => handleChangeColor("steel")}
            >
              steel
            </button>
          </div>
        </div>
        <div className="car mt-2">
          <img className="img-thumbnail" src={state} alt="hinh" />
        </div>
      </div>
    </section>
  );
}
