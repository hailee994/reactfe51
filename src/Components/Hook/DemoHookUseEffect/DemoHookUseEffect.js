import React, { useEffect, useState } from "react";

export default function DemoHookUseEffect(props) {
  const [number, setNumber] = useState(1);

  // useEffect(() => {
  //   //? Hàm này sẽ chạy khi lần đầu component render và các lần render lại
  //   console.log("componentDidMount");
  //   console.log("componentDidUpdate");
  // });

  // useEffect(() => {
  //   //? tham số 2 mảng rổng => chỉ thay thế cho componentDidMount
  //   console.log("componentDidMount");
  // }, []);

  useEffect(() => {
    //? tham số 2 mảng là giá trị đó thay đổi thì hàm này sẽ thực thi
    console.log("componentDidMount");
  }, [number]);

  useEffect(() => {
    return () => {
      //? Hủy thì component sẽ gọi hàm này
      console.log("thay cho componentWillMount");
    };
  });

  return (
    <div>
      <h1>{number}</h1>
      <button onClick={() => setNumber(number + 1)}>+</button>
    </div>
  );
}
