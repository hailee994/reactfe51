import React from "react";
import { useDispatch, useSelector } from "react-redux";

export default function KetQuaF() {
  const { banChon, banThang, banChoi, tongDiem } = useSelector(
    (state) => state.XucXacReducer
  );
  const dispatch = useDispatch();

  return (
    <div>
      <h1>Tong Diem: {tongDiem} </h1>
      <h1>Bạn đã chọn: {banChon === true ? "Tài" : "Xỉu"} </h1>
      <h1>Số bàn chơi: {banChoi} </h1>
      <h1>Số bàn thắng: {banThang} </h1>
      <button
        className="btn btn-success"
        onClick={() => {
          let n = 0;
          let randomXucXac = setInterval(() => {
            let action = {
              type: "GAME_PLAY",
            };
            dispatch(action);
            n++;
            if (n === 5) {
              clearInterval(randomXucXac);
              let action = {
                type: "END_GAME",
              };
              dispatch(action);
            }
          }, 500);
        }}
      >
        Play Game
      </button>
    </div>
  );
}
