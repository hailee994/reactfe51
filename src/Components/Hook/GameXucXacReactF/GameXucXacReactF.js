import React from "react";
import KetQuaF from "./KetQuaF";
import XucXacF from "./XucXacF";

export default function GameXucXacReactF() {
  return (
    <div
      className="container-fluid text-center"
      style={{
        backgroundImage: "url(./img/xucXac/bgGame.png)",
        width: "100%",
        height: "100%",
        position: "fixed",
        top: "0",
        left: "0",
        fontFamily: "fontGame",
      }}
    >
      <h1 className="text-center p-3" style={{ fontSize: "3rem" }}>
        Bai Tap Xuc Xac
      </h1>

      <XucXacF />

      <KetQuaF />
    </div>
  );
}
