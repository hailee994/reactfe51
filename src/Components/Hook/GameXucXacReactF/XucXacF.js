import React from "react";
import { useDispatch, useSelector } from "react-redux";
// import { connect } from "react-redux";

export default function XucXacF(props) {
  //? useSelector là hook thay thế cho phương pháp mapStateToProps của reac-redux
  const { xucXac, banChon } = useSelector((state) => state.XucXacReducer);
  const dispatch = useDispatch();

  const renderXucXac = () => {
    return xucXac.map((xucXac, i) => {
      return (
        <div className="col-4" key={i}>
          <img src={xucXac.hinhAnh} alt="" width={100} height={100} />
        </div>
      );
    });
  };

  const datCuoc = (taiXiu) => {
    let action = {
      type: "DAT_CUOC",
      taiXiu: taiXiu,
    };
    dispatch(action);
    // console.log(taiXiu);
  };

  return (
    <div className="container text-center">
      <div className="row">
        <div className="col-3">
          <button
            className="btn btn-success p-5"
            onClick={() => {
              datCuoc(true);
            }}
          >
            <span className="display-4">Tài</span>
          </button>
        </div>

        <div className="col-6">
          <div className="row">{renderXucXac()}</div>
        </div>
        <div className="col-3">
          <button
            className="btn btn-danger p-5"
            onClick={() => {
              datCuoc(false);
            }}
          >
            <span className="display-4">Xỉu</span>
          </button>
        </div>
      </div>
    </div>
  );
}

// const mapStateToProps = (state) => {
//   return {
//     mangXucXac: state.XucXacReducer.xucXac,
//   };
// };

// export default connect(mapStateToProps)(XucXacF);
