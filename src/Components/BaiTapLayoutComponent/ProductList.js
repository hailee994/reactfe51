import React, { Component } from "react";
import SmartPhone from "./SmartPhone";

export default class ProductList extends Component {
  products = [
    {
      ten: "iphoneX",
      title: "iPhone X features a new all-screen design.",
      imgSrc: "./img/sp_iphoneX.png",
    },
    {
      ten: "Blackberry",
      title: "Blackberry features a new all-screen design.",
      imgSrc: "./img/sp_blackberry.png",
    },
    {
      ten: "Note 7",
      title: "Note 7 features a new all-screen design.",
      imgSrc: "./img/sp_note7.png",
    },
    {
      ten: "Vivo",
      title: "Vivo features a new all-screen design.",
      imgSrc: "./img/sp_vivo850.png",
    },
  ];

  renderSmarthPhone = () => {
    return this.products.map((product, i) => {
      return (
        <SmartPhone
          name={product.ten}
          content={product.title}
          imgPhone={product.imgSrc}
        />
      );
    });
  };

  render() {
    return (
      <div className="container-fluid bg-dark p-5">
        <h1 className="text-white text-center mb-3">BEST SMARTPHONE</h1>
        <div className="row">{this.renderSmarthPhone()}</div>
      </div>
    );
  }
}
