import React, { Component } from "react";
import { connect } from "react-redux";
// import SanPhamGioHangRedux from "./SanPhamGioHangRedux";

class GioHangRedux extends Component {
  renderSPGH = () => {
    return this.props.sanPham.map((sanPham, i) => {
      return (
        <tr key={i}>
          <th>{sanPham.maSP}</th>
          <th>{sanPham.tenSP}</th>
          <th>
            <img src={sanPham.hinhAnh} alt="" width={50} height="50px" />
          </th>
          <th>
            <button onClick={() => this.props.tangGiam(sanPham.maSP, false)}>
              -
            </button>
            {sanPham.soLuong}
            <button onClick={() => this.props.tangGiam(sanPham.maSP, true)}>
              +
            </button>
          </th>
          <th>{sanPham.giaBan}</th>
          <th>{sanPham.soLuong * sanPham.giaBan}</th>
          <th>
            <button
              className="btn btn-danger"
              onClick={() => this.props.xoaSanPham(sanPham.maSP)}
            >
              Delete
            </button>
          </th>
        </tr>
      );
    });
  };

  render() {
    console.log(this.props);
    return (
      <table className="table">
        <thead>
          <tr>
            <th>Ma SP</th>
            <th>Ten SP</th>
            <th>Hinh anh</th>
            <th>So Luong</th>
            <th>Don gia</th>
            <th>Thanh tien</th>
            <th></th>
          </tr>
        </thead>
        <tbody>{this.renderSPGH()}</tbody>
      </table>
    );
  }
}

//? Lấy state từ redux store biến thành props
const mapStateToProps = (state) => {
  return {
    sanPham: state.StateBaiTapGioHang.gioHang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    xoaSanPham: (maSPClick) => {
      // console.log(maSPClick);

      let action = {
        type: "XOA_GIO_HANG",
        maSPClick,
      };

      dispatch(action);
    },

    tangGiam: (maSP, tangGiam) => {
      let action = {
        type: "TANG_GIAM",
        maSP,
        tangGiam,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(GioHangRedux);
