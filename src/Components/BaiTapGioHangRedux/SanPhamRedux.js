// import React, { Component } from "react";

// export default class SanPhamRedux extends Component {
//   render() {
// // let { sanPham } = this.props;
// console.log(this.props);
// return (
// <div className="card">
//   <img
//     className="card-img-top"
//     style={{ width: "100%" }}
//     src={this.props.sanPham.hinhAnh}
//     alt=""
//   />
//   <div className="card-body">
//     <h4 className="card-title">{this.props.sanPham.tenSP}</h4>
//     <p className="card-text">{this.props.sanPham.giaBan}</p>
//     <button className="btn btn-success">Chi tiết</button>
//     <button className="btn btn-danger">Thêm giỏ hàng</button>
//   </div>
// </div>
//     );
//   }
// }

import React, { Component } from "react";
import { connect } from "react-redux";

class SanPhamRedux extends Component {
  render() {
    const { sanPham, themGioHang } = this.props;

    return (
      <div className="card">
        <img
          className="card-img-top"
          style={{ width: "100%" }}
          src={sanPham.hinhAnh}
          alt=""
        />
        <div className="card-body">
          <h4 className="card-title">{sanPham.tenSP}</h4>
          <p className="card-text">{sanPham.giaBan}</p>
          <button className="btn btn-success">Chi tiết</button>
          <button
            className="btn btn-danger"
            onClick={() => themGioHang(sanPham)}
          >
            Thêm giỏ hàng
          </button>
        </div>
      </div>
    );
  }
}

// const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => {
  return {
    themGioHang: (sanPhamClick) => {
      // console.log(sanPham);
      //? Từ sản phẩm được click => tạo ra sp giỏ hàng
      let spGH = { ...sanPhamClick, soLuong: 1 };

      //? Để gửi giá trị lên reducer cần 1 obj có thuộc tính type để phân biệt state nào thay đổi
      let action = {
        type: "THEM_GIO_HANG",
        spGH: spGH,

        //? Dùng hàm dispatch do redux cung cấp
      };
      dispatch(action);
    },
  };
};

//? Tham số 1 hàm connect là 1 hàm (callbackFunction): lấy giá trị từ ruducer về
//? Tham số 2 hàm connect là 1 hàm (callbackFunction): đưa các giá trị lên ruducer
export default connect(null, mapDispatchToProps)(SanPhamRedux);
