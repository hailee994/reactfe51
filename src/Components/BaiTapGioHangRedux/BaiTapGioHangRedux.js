import React, { Component } from "react";
import danhSachSanPham from "./data.json";
import GioHangRedux from "./GioHangRedux";
import SanPhamRedux from "./SanPhamRedux";

export default class BaiTapGioHangRedux extends Component {
  renderSanPham = () => {
    return danhSachSanPham.map((sanPham, i) => {
      return (
        <div className="col-4" key={i}>
          <SanPhamRedux sanPham={sanPham} />
        </div>
      );
    });
  };

  render() {
    return (
      <div className="container">
        <h3 className="text-center">BÀI TẬP GIỎ HÀNG REDUX</h3>
        <div className="">
          <div className="row mb-5">{this.renderSanPham()}</div>

          <p className="text-center">Giỏ hàng (0)</p>
          <div className="gioHang">
            <GioHangRedux />
          </div>
        </div>
      </div>
    );
  }
}
