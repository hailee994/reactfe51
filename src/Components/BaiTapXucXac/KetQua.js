// import React, { Component } from "react";

// export default class KetQua extends Component {
//   render() {
//     return (

//     );
//   }
// }

import React, { Component } from "react";
import { connect } from "react-redux";

class KetQua extends Component {
  render() {
    const { taiXiu, banThang, banChoi, tongDiem } = this.props;

    return (
      <div className="text-center">
        <h1 style={{ fontSize: "3rem" }}>
          Bạn đã chọn:{" "}
          <span style={{ color: "red", fontSize: "3rem" }}>
            {taiXiu ? "Tài" : "Xỉu"}
          </span>
        </h1>

        <h1>
          Tổng điểm:{" "}
          <span style={{ color: "black", fontSize: "3rem" }}>
            {tongDiem} {tongDiem == null ? "" : tongDiem <= 9 ? "Tài" : "Xỉu"}
          </span>
        </h1>

        <h1>
          Số bàn thắng:{" "}
          <span style={{ color: "green", fontSize: "3rem" }}>{banThang}</span>
        </h1>

        <h1>
          Tổng số bàn chơi:{" "}
          <span style={{ color: "blue", fontSize: "3rem" }}>{banChoi}</span>
        </h1>

        <button
          style={{
            backgroundColor: "green",
            color: "#fff",
            padding: "8px 20px",
            fontSize: "1.5rem",
            borderRadius: "10px",
            border: "none",
          }}
          onClick={() => this.props.playGame()}
        >
          Play game
        </button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    taiXiu: state.XucXacReducer.banChon,
    banThang: state.XucXacReducer.banThang,
    banChoi: state.XucXacReducer.banChoi,
    tongDiem: state.XucXacReducer.tongDiem,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    playGame: () => {
      // console.log("game play");
      let n = 0;

      let randomXX = setInterval(() => {
        let action = {
          type: "GAME_PLAY",
        };
        dispatch(action);
        n++;
        if (n === 5) {
          clearInterval(randomXX);
          let actionKQ = {
            type: "END_GAME",
          };
          dispatch(actionKQ);
        }
      }, 500);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(KetQua);
