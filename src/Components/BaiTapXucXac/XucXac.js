import React, { Component } from "react";
import { connect } from "react-redux";

class XucXac extends Component {
  renderXucXac = () => {
    return this.props.xucXac.map((xucXac, i) => {
      return (
        <img
          key={i}
          src={xucXac.hinhAnh}
          alt=""
          width={"100px"}
          height={"100px"}
        />
      );
    });
  };

  render() {
    console.log(this.props);
    return (
      <div className="row text-center mb-3">
        <div className="col-4">
          <button
            className="btn btn-success p-5"
            onClick={() => this.props.datCuoc(true)}
          >
            <span className="display-4">Tài</span>
          </button>
        </div>

        <div className="col-4">{this.renderXucXac()}</div>

        <div className="col-4">
          <button
            className="btn btn-success p-5"
            onClick={() => this.props.datCuoc(false)}
          >
            <span className="display-4">Xỉu</span>
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    xucXac: state.XucXacReducer.xucXac,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    datCuoc: (taiXiu) => {
      console.log(taiXiu);
      let action = {
        type: "DAT_CUOC",
        taiXiu,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(XucXac);
