/**
 * b1: Xây dựng giao diện
 * b2: Xác định nguồn dữ liệu thay đổi:
 *    + mangXucXac:[{}]
 *    + banChon: "Tai"
 *    + soBanThang: 0,
 *    + soNguoiChoi: 0
 */
import React, { Component } from "react";
import "./BaiTapXucXac.module.scss";
import KetQua from "./KetQua";
import XucXac from "./XucXac";

export default class BaiTapXucXac extends Component {
  render() {
    return (
      <div
        className="container-fluid baiXucXac"
        style={{
          backgroundImage: "url(./img/xucXac/bgGame.png)",
          width: "100%",
          height: "100%",
          position: "fixed",
          top: "0",
          left: "0",
        }}
      >
        <h1 className="text-center p-3" style={{ fontSize: "3rem" }}>
          Bai Tap Xuc Xac
        </h1>

        <XucXac />

        <KetQua />
      </div>
    );
  }
}
