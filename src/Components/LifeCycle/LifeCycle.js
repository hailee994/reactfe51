import React, { Component } from "react";
import ChildComponent from "./ChildComponent";

export default class LifeCycle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: 1,
    };
    console.log("Constructor");
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    // Rarely Used
    console.log("getDerivedStateFromProps");
    return null;
  }

  render() {
    console.log("render");
    return (
      <div>
        123
        <h1>LifeCycle Number: {this.state.number}</h1>
        <button
          onClick={() =>
            this.setState({
              number: (this.state.number += 1),
            })
          }
        >
          +
        </button>
        {/* {this.state.number < 3 ? <ChildComponent /> : ""} */}
        <ChildComponent number={this.state.number} />
      </div>
    );
  }

  componentDidMount() {
    //? gọi api tại did mount
    console.log("componentDidMount");
  }

  //? hàm này chạy khi setState thay đổi
  componentDidUpdate(prevProps, prevState) {
    console.log("componentDidUpdate");
  }
}
