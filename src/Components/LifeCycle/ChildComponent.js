import React, { PureComponent } from "react";
//? pure component giống vs component chỉ khác ở life cycle k có should component update và chỉ so sánh dữ liệu kiểu nguyên thủy

export default class ChildComponent extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
    console.log("Constructor child");
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    // Rarely Used
    console.log("getDerivedStateFromProps child");
    return null;
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log("shouldComponentUpdate");
  //   return false;
  // }

  render() {
    console.log("render child");
    return (
      <div>
        <h3>{this.props.number}</h3>
        Component child
      </div>
    );
  }

  componentDidMount() {
    console.log("componentDidMount child");
  }

  componentDidUpdate(prevProps, prevState) {
    console.log("componentDidUpdate child");
  }

  componentWillUnmount() {
    //? life cycle chạy trước khi component mất khỏi giao diện
    console.log("componentWillUnmount");
  }
}
