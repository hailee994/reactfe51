// import React, { Component } from "react";

// export default class DanhSachNguoiDung extends Component {
//   render() {
//     return (

//     );
//   }
// }

import React, { Component } from "react";
import { connect } from "react-redux";
import {
  chinhSuaNguoiDungAction,
  xoaNguoiDungAction,
} from "../../redux/action/QuanLyNguoiDungAction";
import FormComponent from "./FormComponent";
import NewFormComponent from "./NewFormComponent";

export class DanhSachNguoiDung extends Component {
  renderDanhSachNguoiDung = () => {
    return this.props.danhSachNguoiDung.map((item, i) => {
      return (
        <tr key={i}>
          <th>{item.maNguoiDung}</th>
          <th>{item.tenNguoiDung}</th>
          <th>{item.soDienThoai}</th>
          <th>{item.email}</th>
          <th>
            <button
              className="btn btn-primary mr-3"
              onClick={() => {
                let action = chinhSuaNguoiDungAction(item);
                this.props.dispatch(action);
              }}
            >
              Edit
            </button>
            <button
              className="btn btn-danger"
              onClick={() => {
                //   //?  Gọi hàm trong mapDispatchToProps
                //   //? Hàm này khi sử dụng connect với redux sẽ tự có trong props này
                //   let action = {
                //     type: "XOA_NGUOI_DUNG",
                //     maNguoiDung: item.maNguoiDung,
                //   };
                //   this.props.dispatch(action);
                this.props.dispatch(xoaNguoiDungAction(item.maNguoiDung));
              }}
            >
              Delete
            </button>
          </th>
        </tr>
      );
    });
  };

  render() {
    return (
      <div className="container">
        {/* <FormComponent /> */}
        <NewFormComponent />
        <table className="table mt-5">
          <thead>
            <tr>
              <th>Mã người dùng</th>
              <th>Tên người dùng</th>
              <th>Số điện thoại</th>
              <th>Email</th>
              <th></th>
            </tr>
          </thead>

          <tbody>{this.renderDanhSachNguoiDung()}</tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log(state);
  return {
    danhSachNguoiDung: state.QuanLyNguoiDungReducer.mangNguoiDung,
  };
};

export default connect(mapStateToProps)(DanhSachNguoiDung);
