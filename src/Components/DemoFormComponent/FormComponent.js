import React, { Component } from "react";
import note from "sweetalert2";
import { connect } from "react-redux";
import { xoaNguoiDungAction } from "../../redux/action/QuanLyNguoiDungAction";

class FormComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      values: {
        maNguoiDung: "",
        tenNguoiDung: "",
        soDienThoai: "",
        email: "",
      },
      errors: {
        maNguoiDung: "",
        tenNguoiDung: "",
        soDienThoai: "",
        email: "",
      },
      maNguoiDungXoa: "",
    };
  }

  handleChangeInput = (event) => {
    //! Lấy ra name và value
    let { name, value } = event.target;

    //! Lấy ra attribute types (các thuộc tính trên thẻ tự thêm gọi là attribute)
    let types = event.target.getAttribute("types");
    console.log(types);

    let newValue = { ...this.state.values }; //? Tạo ra value mới
    newValue[name] = value;

    //? Xử lý errors
    let newError = { ...this.state.errors };
    newError[name] = value.trim() === "" ? "Không được bỏ trống" : "";

    if (types === "phoneNumber") {
      const regexNumber = /^[0-9]+$/;
      if (!regexNumber.test(value.trim())) {
        newError[name] = "Dữ liệu không hợp lệ";
      }
    }

    if (types === "mail") {
      const regexMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!regexMail.test(value.trim())) {
        newError[name] = "Dữ liệu không hợp lệ";
      }
    }

    this.setState(
      {
        values: newValue,
        errors: newError,
      },
      () => {
        console.log(this.state);
      }
    );
  };

  //* Giải pháp 1 sử dụng lifecycle componentWillReceiveProps
  //* Hàm này chạy khi props thay đổi và trước khi render
  componentWillReceiveProps(newProps) {
    //* khi bấm bút sửa => props thay đổi ta lấy dc props từ ng dung chỉnh sửa (this.props.nguoidungchinhsua) ta gán vào state cua component => va render value từ state
    this.setState({
      values: newProps.nguoiDungChinhSua,
    });
  }

  render() {
    //* lấy dữ liệu QuanLyNguoiDungReducer.nguoiDungChinhSua về render lên value
    let { maNguoiDung, tenNguoiDung, soDienThoai, email } = this.state.values;

    return (
      <form
        className="card text-left"
        onSubmit={(event) => {
          //! Cản submit lại trang của browser
          event.preventDefault();

          let valid = true;

          //? Duyệt thuộc tính trong object values (duyệt thuộc tính trong đối tượng thì dùng es6 for in)
          for (let tenThuocTinh in this.state.values) {
            if (this.state.values[tenThuocTinh].trim() === "") {
              valid = false;
            }
          }

          for (let tenThuocTinh in this.state.errors) {
            if (this.state.errors[tenThuocTinh].trim() !== "") {
              valid = false;
            }
          }

          if (!valid) {
            note.fire("Thông báo", "Dữ liệu không hợp lệ", "error");
            return;
          }

          note.fire("Thông báo", "Thêm người dùng thánh công");
          console.log("submit");
        }}
      >
        <div className="card-header bg-dark text-light font-weight-both text-center">
          <h2 className="card-title">Thông tin người dùng</h2>
        </div>
        <div className="card-body">
          <div className="row">
            <div className="col-6">
              <div className="form-group">
                <span>Mã người dùng</span>
                <input
                  className="form-control"
                  name="maNguoiDung"
                  onChange={this.handleChangeInput}
                  value={maNguoiDung}
                />
                <p className="text-danger">{this.state.errors.maNguoiDung}</p>
              </div>

              <div className="form-group">
                <span>Tên người dùng</span>
                <input
                  className="form-control"
                  name="tenNguoiDung"
                  onChange={this.handleChangeInput}
                  value={tenNguoiDung}
                />
                <p className="text-danger">{this.state.errors.tenNguoiDung}</p>
              </div>
            </div>

            <div className="col-6">
              <div className="form-group">
                <span>Số điện thoại</span>
                <input
                  types="phoneNumber"
                  className="form-control"
                  name="soDienThoai"
                  onChange={this.handleChangeInput}
                  value={soDienThoai}
                />
                <p className="text-danger">{this.state.errors.soDienThoai}</p>
              </div>

              <div className="form-group">
                <span>Email</span>
                <input
                  types="mail"
                  className="form-control"
                  name="email"
                  onChange={this.handleChangeInput}
                  value={email}
                />
                <p className="text-danger">{this.state.errors.email}</p>
              </div>
            </div>
          </div>
        </div>

        <div className="col-12 text-right mb-3">
          <button className="btn btn-success">Thêm người dùng</button>
        </div>

        <div className="col-12 text-right mb-3">
          <input
            name="maNguoiDungXoa"
            placeholder="Nhập mã người dùng cần xóa"
            className="form-control"
            onChange={(e) => {
              this.setState({
                maNguoiDungXoa: e.target.value,
              });
            }}
          />

          <button
            type="button"
            className="btn btn-danger mt-3"
            onClick={() => {
              let action = xoaNguoiDungAction(this.state.maNguoiDungXoa);
              //{
              //   type: "XOA_NGUOI_DUNG",
              //   maNguoiDung: this.state.maNguoiDungXoa,
              // };
              this.props.dispatch(action);
            }}
          >
            Xóa
          </button>
        </div>
      </form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    nguoiDungChinhSua: state.QuanLyNguoiDungReducer.nguoiDungChinhSua,
  };
};

export default connect(mapStateToProps)(FormComponent);
