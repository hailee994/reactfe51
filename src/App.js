import React from "react";
import "./App.scss";
import DemoHookUseEffect from "./Components/Hook/DemoHookUseEffect/DemoHookUseEffect";
import DemoUseCallBack from "./Components/Hook/DemoUseCallBack/DemoUseCallBack";
import DemoHookUseMemo from "./Components/Hook/DemoUseMemo/DemoHookUseMemo";
import GameXucXacReactF from "./Components/Hook/GameXucXacReactF/GameXucXacReactF";
// import BaiTapDoiMauXe from "./Components/Hook/DemoHookUseState/BaiTapDoiMauXe";
// import DanhSachNguoiDung from "./Components/DemoFormComponent/DanhSachNguoiDung";
// import DemoHookUseState from "./Components/Hook/DemoHookUseState/DemoHookUseState";
// import ChonMauXe from "./Components/BaiTapChonMauXe";
// import BaiTapGioHang from "./Components/BaiTapGioHang";
// import BaiTapGioHangRedux from "./Components/BaiTapGioHangRedux/BaiTapGioHangRedux";
// import BaiTapXucXac from "./Components/BaiTapXucXac/BaiTapXucXac";
// import LifeCycle from "./Components/LifeCycle/LifeCycle";
// import BaiTap1 from "./Components/BaiTapLayoutComponent/BaiTapLayout1";
// import DataBinding from "./Components/DataBinding/DataBinding";
// import DemoConditionalAndState from "./Components/DemoLogin/DemoConditionalAndState";
// import HandelEvent from "./Components/HandelEvent/HandelEvent";
// import DemoListAndKeys from "./Components/ListAndKeys/DemoListAndKeys";
// import DemoProps from "./Components/Props/DemoProps";

function App() {
  //! Bên trong lệnh return của function component là nội dung giao diện của thẻ này. Lưu ý: Nội dung component phải đc bao phủ bởi 1 thẻ bất kỳ
  return (
    <div className="App">
      {/* <BaiTap1 />
      <DataBinding />
      <HandelEvent />
      ------------------------------Buoi
      2--------------------------------------------
      <DemoConditionalAndState />
      <ChonMauXe />
      <DemoListAndKeys />
      <DemoProps /> */}
      {/* <BaiTapGioHang /> */}
      {/* <BaiTapGioHangRedux /> */}
      {/* <BaiTapXucXac /> */}
      {/* <LifeCycle /> */}
      {/* <DanhSachNguoiDung /> */}
      {/* <DemoHookUseState /> */}
      {/* <BaiTapDoiMauXe /> */}
      {/* <DemoHookUseEffect /> */}
      {/* <DemoUseCallBack /> */}
      {/* <DemoHookUseMemo /> */}
      <GameXucXacReactF />
    </div>
  );
}

export default App;
