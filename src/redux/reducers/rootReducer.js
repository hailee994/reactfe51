//? Khai báo tất cả các state của ứng dụng
import { combineReducers } from "redux";
// import BaiTapGioHangReducer from "./BaiTapGioHangReducer";
import BaiTapXucXacReducer from "./BaiTapXucXacReducer";
// import { QuanLyNguoiDungReducer } from "./QuanLyNguoiDungReducer";

//? state tổng của ứng dụng
export const rootReducer = combineReducers({
  //? Nơi khai báo các state theo từng nghiệp vụ
  // StateBaiTapGioHang: BaiTapGioHangReducer,
  XucXacReducer: BaiTapXucXacReducer,
  // QuanLyNguoiDungReducer: QuanLyNguoiDungReducer,
});
