const initialState = {
  banChon: true,
  xucXac: [
    { ma: 1, hinhAnh: "./img/xucXac/1.png" },
    { ma: 1, hinhAnh: "./img/xucXac/1.png" },
    { ma: 1, hinhAnh: "./img/xucXac/1.png" },
  ],
  banThang: 0,
  banChoi: 0,
  // tongDiem: 0,
};

const BaiTapXucXacReducer = (state = initialState, action) => {
  switch (action.type) {
    case "DAT_CUOC": {
      state.banChon = action.taiXiu;
      console.log("nguoi choi chon ->>", state.banChon);

      return { ...state };
    }

    case "GAME_PLAY": {
      let arrXucXac = [];

      for (let i = 0; i < 3; i++) {
        let numNgauNhien = Math.floor(Math.random() * 6) + 1;
        arrXucXac.push({
          ma: numNgauNhien,
          hinhAnh: `./img/xucXac/${numNgauNhien}.png`,
        });
      }
      state.xucXac = arrXucXac;

      // let tongDiem = arrXucXac.reduce((tong, xucXac, i) => {
      //   return (tong += xucXac.ma);
      // }, 0);

      // if (
      //   (tongDiem < 11 && action.taiXiu) ||
      //   (tongDiem > 11 && !action.taiXiu)
      // ) {
      //   state.banThang += 1;
      // }
      return { ...state };
    }

    case "END_GAME": {
      let tongDiem = state.xucXac.reduce((tong, item, i) => {
        return (tong += item.ma);
      }, 0);
      // state.tongDiem = tongDiem;
      if ((tongDiem < 9 && !state.banChon) || (tongDiem > 9 && state.banChon)) {
        state.banThang += 1;
      }
      state.banChoi += 1;

      return { ...state, tongDiem: tongDiem };
    }

    default:
      return state;
  }
};

export default BaiTapXucXacReducer;
