import {
  CHINH_SUA_NGUOI_DUNG,
  XOA_NGUOI_DUNG,
} from "../constants/QuanLyNguoiDung";

const initialState = {
  stateForm: {
    values: {
      maNguoiDung: "",
      tenNguoiDung: "",
      soDienThoai: "",
      email: "",
    },
    errors: {
      maNguoiDung: "",
      tenNguoiDung: "",
      soDienThoai: "",
      email: "",
    },
    maNguoiDungXoa: "",
  },

  mangNguoiDung: [
    {
      maNguoiDung: 1,
      tenNguoiDung: "Nguyen Van A",
      soDienThoai: "111111111",
      email: "ngVanA@gmail.com",
    },
    {
      maNguoiDung: 2,
      tenNguoiDung: "Nguyen Van A",
      soDienThoai: "111111111",
      email: "ngVanA@gmail.com",
    },
  ],
  nguoiDungChinhSua: {},
};

export const QuanLyNguoiDungReducer = (state = initialState, action) => {
  switch (action.type) {
    case XOA_NGUOI_DUNG:
      let mangNguoiDungUpdate = [...state.mangNguoiDung];
      mangNguoiDungUpdate = mangNguoiDungUpdate.filter(
        (item) => item.maNguoiDung != action.maNguoiDung
      );

      state.mangNguoiDung = mangNguoiDungUpdate;
      return { ...state };

    case CHINH_SUA_NGUOI_DUNG: {
      //* Lấy người dùng được click
      let nguoiDungClick = { ...action.nguoiDungChinhSua };
      state.nguoiDungChinhSua = nguoiDungClick;

      state.stateForm = { ...state.stateForm, values: nguoiDungClick };
      return { ...state };
    }

    case "HANDLE_CHANGE_INPUT": {
      state.stateForm = { ...action.newState };
      return { ...state };
    }

    case "CAP_NHAT": {
      let mangNguoiDungCapNhat = [...state.mangNguoiDung];

      let index = mangNguoiDungCapNhat.findIndex(
        (item) => item.maNguoiDung === action.nguoiDungCapNhat.maNguoiDung
      );

      mangNguoiDungCapNhat[index] = action.nguoiDungCapNhat;
      state.mangNguoiDung = mangNguoiDungCapNhat;
      return { ...state };
    }

    case "THEM_NGUOI_DUNG": {
      let mangNguoiDungUpdate = [...state.mangNguoiDung];
      mangNguoiDungUpdate.push(action.themNguoiDung);

      state.mangNguoiDung = mangNguoiDungUpdate;
      return { ...state };
    }

    default:
      return state;
  }
};
