const stateDefault = {
  gioHang: [
    // {
    //   maSP: 1,
    //   tenSP: "Iphone",
    //   hinhAnh: "https://picsum.photos/50",
    //   soLuong: 1,
    // },
  ],
};

const BaiTapGioHangReducer = (state = stateDefault, action) => {
  switch (action.type) {
    case "THEM_GIO_HANG": {
      let gioHangUpdate = [...state.gioHang];

      // const index = gioHangUpdate.findIndex(
      //   (spGH) => spGH.maSP === action.spGH.maSP
      // );

      const sp = gioHangUpdate.filter(
        (sanPham) => sanPham.maSP === action.spGH.maSP
      );
      if (sp != "") {
        sp.soLuong += 1;
      } else {
        gioHangUpdate.push(action.spGH);
      }
      state.gioHang = gioHangUpdate;
      return { ...state };
    }

    case "XOA_GIO_HANG": {
      let gioHangUpdate = [...state.gioHang];

      const gioHangXoa = gioHangUpdate.filter(
        (sanPham) => sanPham.maSP !== action.maSPClick
      );

      state.gioHang = gioHangXoa;
      return { ...state };
    }

    case "TANG_GIAM": {
      let gioHangUpdate = [...state.gioHang];
      const slTangGiam = gioHangUpdate.find(
        (sanPham) => sanPham.maSP === action.maSP
      );

      if (slTangGiam) {
        if (action.tangGiam) {
          slTangGiam.soLuong += 1;
        } else {
          if (slTangGiam.soLuong > 1) {
            slTangGiam.soLuong -= 1;
          }
        }
      }

      state.gioHang = gioHangUpdate;
      return { ...state };
    }

    default:
      return state;
  }
};

export default BaiTapGioHangReducer;
