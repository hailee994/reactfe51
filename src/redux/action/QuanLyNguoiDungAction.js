import {
  CHINH_SUA_NGUOI_DUNG,
  XOA_NGUOI_DUNG,
} from "../constants/QuanLyNguoiDung";

//? Định nghĩa action để các component trong ứng dụng muốn gọi nghiệp vụ này thì import vao gọi
export const xoaNguoiDungAction = (maNguoiDung) => {
  return {
    type: XOA_NGUOI_DUNG,
    maNguoiDung,
  };
};

export const chinhSuaNguoiDungAction = (nguoiDung) => {
  return {
    type: CHINH_SUA_NGUOI_DUNG,
    nguoiDungChinhSua: nguoiDung,
  };
};
